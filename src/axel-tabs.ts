import { AxelTabsStatic } from "./axel"
import { AxelTabsObject, AxelTabsOptions, AxelTabsUserOptions } from "./axel-types"

/**
 * Initializes and controls list of tab groups.
 */
export class AxelTabs extends AxelTabsStatic {
  /**
   * Whether the first tab group has been initialized
   */
  private static initialized: boolean = false
  /**
   * Initialize new tab group and add it to the static list
   * 
   * @param selector tab group selector
   * @param userOptions allows to specify `name` and `defaultOpenedTab`
   * @returns object of a single tab group that has been initialized 
   */
  static init(selector: string, userOptions: AxelTabsUserOptions = {}): AxelTabsObject {
    const defaultOptions: AxelTabsOptions = {
      name: null,
      defaultOpenedTab: null
    }

    const controlsSelector = '[data-axel-tabs="controls"]'
    const panelsSelector = '[data-axel-tabs="panels"]'
    const tabsSelectors = '[data-axel-tabs*="tab-"]'

    // add styles on first tab group initialization
    if (!this.initialized) {
      const style = document.createElement('style')
      style.textContent = panelsSelector + ' ' + tabsSelectors + ':not(.axel-active) { display: none }'
      document.head.appendChild(style)
      this.initialized = true
    }

		const element = document.querySelector(selector) as HTMLDivElement
    const ctr = element.querySelector(controlsSelector) as HTMLDivElement
    const pan = element.querySelector(panelsSelector) as HTMLDivElement
    const controls = Array.from(ctr.querySelectorAll(tabsSelectors)) as HTMLDivElement[]
    const panels = Array.from(pan.querySelectorAll(tabsSelectors)) as HTMLDivElement[]
    const options: AxelTabsOptions = { ...defaultOptions, ...userOptions }
    const tabToOpen = options.defaultOpenedTab

    const openTab = (tabName: string) => {  
			tabsObj.panels.forEach(tab => {
				if (tab.attributes.getNamedItem('data-axel-tabs')?.value === tabName) {
          tab.classList.add('axel-active')
        } else {
          tab.classList.remove('axel-active')
        }
			})
			tabsObj.controls.forEach(ctr => {
				if (ctr.attributes.getNamedItem('data-axel-tabs')?.value === tabName) {
          ctr.classList.add('axel-active')
        } else {
          ctr.classList.remove('axel-active')
        }
			})
		}

    // create object for html tab group
		const tabsObj: AxelTabsObject = {
      element: element,
			controls: controls,
      panels: panels,
      options: options,
      openedTabName: tabToOpen,
			openTab: openTab
    }

		// set initial state
		tabToOpen && openTab(tabToOpen)

    // append event listeners for controls
		tabsObj.controls.forEach(el => {
			el.addEventListener('click', (ev => {
        const tabName = el.attributes.getNamedItem('data-axel-tabs')?.value as string
        tabsObj.openTab(tabName);
        tabsObj.openedTabName = tabName;
			}))
		})

    // add tabs object to the static list
		this.allObjects.push(tabsObj)

		return tabsObj
  }

  /**
   * Opens a tab in specific tab group
   * 
   * @param tabGroupName
   * @param tabName
   */
	static openTab(tabGroupName: string, tabName?: string) {
    const tabs = this.getObject(tabGroupName)
    tabs.openTab(tabName)
  }
}