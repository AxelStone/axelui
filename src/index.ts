import { AxelSelect } from './axel-select';
import { AxelSwitcher } from './axel-switcher';
import { AxelAccordion } from './axel-accordion';
import { AxelColorPicker } from './axel-colorpicker';
import { AxelSlider } from './axel-slider';
import { AxelTabs } from './axel-tabs';
import { AxelLayout } from './axel-layout';

(window as any).AxelSelect = AxelSelect;
// (window as any).AxelSwitcher = AxelSwitcher;
(window as any).AxelAccordion = AxelAccordion;
// (window as any).AxelColorPicker = AxelColorPicker;
// (window as any).AxelSlider = AxelSlider;
// (window as any).AxelTabs = AxelTabs;
// (window as any).AxelLayout = AxelLayout;