import { AxelAccordionStatic, AxelColorPickerStatic, AxelLayoutStatic, AxelSelectStatic, AxelSliderStatic, AxelSwitcherStatic, AxelTabsStatic } from "./axel"

/**
 * Initialized Axel UI element's object
 */
type AxelElementObjectBase = {
  /**
   * Main HTML DOM element of initialized object
   */
  element: HTMLElement 
  /**
   * Options defining behaviour and a state of the initialized object
   */ 
  options: AxelElementOptionsBase,
}

/**
 * Options defining behaviour and a state of the initialized object
 */
type AxelElementOptionsBase = { name: string | null }

/**
 * AxelLayout
 */
declare class AxelLayout extends AxelLayoutStatic {
  init(
    selector: string,
    options?: AxelLayoutOptions,
    onDebouncedResize?: (event: Event) => void
  ): AxelLayoutObject
}
type AxelLayoutObject = AxelElementObjectBase & {
  options: AxelLayoutOptions
  contentElement: HTMLElement
  onDebouncedResize: (event: Event) => void
}
type AxelLayoutOptions = AxelElementOptionsBase & {
  layoutMinHeight: 'screen' | 'auto' | number | null,
  contentElement: string | null,
  keepContentMinHeightEqualTo: string | null,
  debounceTimeInMs: number,
}
type AxelLayoutUserOptions = Partial<AxelLayoutOptions>

/**
 * AxelSwitcher
 */
declare class AxelSwitcher extends AxelSwitcherStatic {
  init(selector: string, onSwitch: (state: string) => void, options: AxelSwitcherOptions): AxelSwitcherObject
  switch(state?: 'on' | 'neutral' | 'off'): void
}
type AxelSwitcherObject = AxelElementObjectBase & {
  options: AxelSwitcherOptions
  control: HTMLElement
  state: 'on' | 'off' | 'neutral'
  switch: (state?: 'on' | 'neutral' | 'off') => 'on' | 'off' | 'neutral'
}
type AxelSwitcherOptions = AxelElementOptionsBase & {
  defaultState: 'off' | 'on' | 'neutral'
  mode: 'binary' | 'ternary'
}
type AxelSwitcherUserOptions = Partial<AxelSwitcherOptions>

/**
 * AxelSlider
 */
declare class AxelSlider extends AxelSliderStatic {
  init(selector: string, options: AxelSliderOptions): AxelSliderObject
  slideTo: (pageIndex: number) => void
}
type AxelSliderObject = AxelElementObjectBase & {
  options: AxelSliderOptions
  currentPage: number
  pages: HTMLDivElement[]
  controls: {
    elArrowLeft: HTMLElement
    elArrowRight: HTMLElement
    elCurrentPage: HTMLElement
    elPageTotal: HTMLElement
  }
  slideTo: (index: number) => void
}
type AxelSliderOptions = AxelElementOptionsBase & {
  startAt: number
}
type AxelSliderUserOptions = Partial<AxelSliderOptions>

/**
 * AxelTabs
 */
declare class AxelTabs extends AxelTabsStatic {
  init(selector: string, options: AxelTabsOptions): AxelTabsObject
  openTab: (tabName: string) => void
}
type AxelTabsObject = AxelElementObjectBase & {
  options: AxelTabsOptions
  controls: HTMLDivElement[]
  panels: HTMLDivElement[]
  openedTabName: string | null
  openTab: (tabName: string) => void
}
type AxelTabsOptions = AxelElementOptionsBase & {
  defaultOpenedTab: string | null
}
type AxelTabsUserOptions = Partial<AxelTabsOptions>

/**
 * AxelAccordion
 */
declare class AxelAccordion extends AxelAccordionStatic {
  init(selector: string, options: AxelAccordionOptions): AxelAccordionObject
  switchTab: (tabName: string) => void
  closeAllTabs: () => void
}
type AxelAccordionObject = AxelElementObjectBase & {
  options: AxelAccordionOptions
  tabs: HTMLDivElement[]
  openedTabNames: string[]
  switchTab: (tabName: string) => void
  closeAllTabs: () => void
}
type AxelAccordionOptions = AxelElementOptionsBase & {
  allowMultiple: boolean
  defaultOpenedTabs: string[]
}
type AxelAccordionUserOptions = Partial<AxelAccordionOptions>

/**
 * AxelSelect
 */
declare class AxelSelect extends AxelSelectStatic {
  init: AxelSelectInitFn
  switchOption: AxelSelectSwitchOptionFn
}
type AxelSelectInitFn = (
  selector: string, 
  options: AxelSelectOptions,
  onSwitch?: (
    optionName: string,
    newState: boolean,
    selectedOptions: string[],
    optionEl: HTMLDivElement
  ) => void 
) => AxelSelectObject
type AxelSelectSwitchOptionFn = (
  optionName: string, 
  newState?: boolean, 
  triggerCallback?: boolean,
) => void
type AxelSelectSwitchOptionsFn = (
  optionNames: string[], 
  newState?: boolean, 
  triggerCallbacks?: boolean,
) => void
type AxelSelectSwitchOptionStaticFn = (
  optionName: string, 
  newState?: boolean, 
  selectName?: string,
) => void
type AxelSelectSwitchOptionsStaticFn = (
  optionNames: string[], 
  newState?: boolean, 
  selectName?: string,
) => void
type AxelSelectObject = AxelElementObjectBase & {
  options: AxelSelectOptions
  selectedOptions: string[]
  listeners: {
    onControlClick: () => void
    onClickOutside: (event: MouseEvent) => void
    options: Record<string, () => void>
  }
  switchOption: AxelSelectSwitchOptionFn
  switchOptions: AxelSelectSwitchOptionsFn
}
type AxelSelectOptions = AxelElementOptionsBase & {
  allowMultiple: boolean
  closeOnSelect: boolean
  textEmpty: string
  textFull: string | null
  defaultSelectedOptions: string[]
}
type AxelSelectUserOptions = Partial<AxelSelectOptions>

/**
 * AxelColorPicker
 */
declare class AxelColorPicker extends AxelColorPickerStatic {
  init(
    selector: string,
    onSelect: (color: string | null, colorPickerName: string | null) => void,
    options: AxelColorPickerOptions
  ): AxelColorPickerObject
}
type AxelColorPickerObject = AxelElementObjectBase & {
  options: AxelColorPickerOptions,
  controls: HTMLElement[],
  selectedColor: string | null,
  selectColor: (color: string) => void
}
type AxelColorPickerOptions = AxelElementOptionsBase & {
  colorGroups: string[][]
  defaultSelectedColor: string | null
}
type AxelColorPickerUserOptions = Partial<AxelColorPickerOptions>