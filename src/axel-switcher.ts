import { AxelAccordionStatic, AxelSwitcherStatic } from "./axel";
import { AxelAccordionObject, AxelAccordionOptions, AxelAccordionUserOptions, AxelSwitcherObject, AxelSwitcherOptions, AxelSwitcherUserOptions } from "./axel-types";

export class AxelSwitcher extends AxelSwitcherStatic {
  private static initialized: boolean = false
  private static defaultOptions: AxelSwitcherOptions = {
    name: null,
    defaultState: 'off',
    mode: 'binary'
  }

  static init(
    selector: string,
    onSwitch: (state: string, switcherName: string | null) => void,
    userOptions: AxelSwitcherUserOptions = {}
  ): AxelSwitcherObject {
    const controlSelector = '[data-axel-switcher="control"]'

    // add styles on first switcher initialization
    if (!this.initialized) {
      const style = document.createElement('style')
      const line1 = selector + ' { min-width: 15px; display: inline-block; } '
      const line2 = selector + '.axel-on { text-align: right; } '
      const line3 = selector + '.axel-neutral { text-align: center; } '
      const line4 = selector + '.axel-off { text-align: left; } '
      const line5 = selector + ' [data-axel-switcher="control"] { display: inline-block; min-width: 10px; min-height: 10px; } '
      style.textContent = line1 + line2 + line3 + line4 + line5
      document.head.appendChild(style)
      this.initialized = true
    }

    const element = document.querySelector(selector) as HTMLElement
    const existingControl = element.querySelector(controlSelector) as HTMLElement
    const options: AxelSwitcherOptions = { ...this.defaultOptions, ...userOptions }

    // append control element if it is not defined in HTML
    const control = existingControl ?? (() => {
      const generatedControl = document.createElement('span') as HTMLElement
      generatedControl.setAttribute('data-axel-switcher', 'control')
      element.appendChild(generatedControl)

      return generatedControl
    })()

    const switchState = (state: 'on' | 'off' | 'neutral' | undefined = undefined): 'on' | 'off' | 'neutral' => {
      const switchToState = state ?? (() => {
        if (switcherObj.state === 'on') return 'off'
        else if (switcherObj.state === 'neutral') return 'on'
        else return switcherObj.options.mode === 'binary' ? 'on' : 'neutral'
      })()

      if (switchToState === 'on') {
        switcherObj.element.classList.add('axel-on')
        switcherObj.element.classList.remove('axel-neutral')
        switcherObj.element.classList.remove('axel-off')
      } else if (switchToState === 'neutral') {
        switcherObj.element.classList.remove('axel-on')
        switcherObj.element.classList.add('axel-neutral')
        switcherObj.element.classList.remove('axel-off')
      } else if (switchToState === 'off') {
        switcherObj.element.classList.remove('axel-on')
        switcherObj.element.classList.remove('axel-neutral')
        switcherObj.element.classList.add('axel-off')
      }

      switcherObj.state = switchToState
      onSwitch(switchToState, switcherObj.options.name)

      return switchToState
    }

    // create object for html switcher
		const switcherObj: AxelSwitcherObject = {
      element: element,
      control: control,
      options: options,
      state: options.defaultState,
			switch: switchState,
    }

		// set initial state
    switcherObj.switch(switcherObj.state)

    // append event listeners for controls
		switcherObj.element.addEventListener('click', (ev => {
      const newState = switcherObj.switch()
      onSwitch(newState, switcherObj.options.name)
		}))

    // add accordion object to the static `allAccordions` list
		this.allObjects.push(switcherObj)

		return switcherObj
  }

  static switch(switcherName: string, state?: 'on' | 'off' | 'neutral') {
    const switcher = this.getObject(switcherName)
    switcher.switch(state)
  }
}