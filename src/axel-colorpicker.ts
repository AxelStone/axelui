import { AxelColorPickerStatic } from "./axel"
import { AxelColorPickerObject, AxelColorPickerOptions, AxelColorPickerUserOptions } from "./axel-types"

export class AxelColorPicker extends AxelColorPickerStatic {
  /**
   * Initialize new color picker and add it to the static list
   * 
   * @param selector color picker selector
   * @param userOptions allows to specify `name`, `colorGroups` and `defaultSelectedColor`
   * @returns object of a single color picker that has been initialized 
   */
  static init(
    selector: string, 
    onSelect: (color: string, colorPickerName: string | null) => void, 
    userOptions: AxelColorPickerUserOptions = {}
  ): AxelColorPickerObject {
    const defaultOptions: AxelColorPickerOptions = {
      name: null,
      defaultSelectedColor: null,
      colorGroups: [
        ["#000000", "#808080", "#660033", "#003300", "#000066", "#663300", "#660066", "#003366"],
        ["#ffffff", "#cccccc", "#ff99cc", "#ccffcc", "#9999ff", "#ffffcc", "#ffccff", "#ccffff"]
      ]
    }

		const element = document.querySelector(selector) as HTMLDivElement
    const options: AxelColorPickerOptions = { ...defaultOptions, ...userOptions }
    const colorToSelect = options.defaultSelectedColor

    const selectColor = (color: string) => {  
      const sanitizedColor = color.toLowerCase();
			colorPickerObj.controls.forEach(control => {
				if (control.attributes.getNamedItem('data-axel-colorpicker')?.value === sanitizedColor) {
          control.classList.add('axel-active')
        } else {
          control.classList.remove('axel-active')
        }
			})
		}

    // create grouped colors HTML
    const controls = [] as HTMLElement[];
    options.colorGroups.forEach(colorGroup => {
      const colorGroupElement = document.createElement('div')
      colorGroupElement.style.cssText = 'display: flex;'
      colorGroup.forEach(color => {
        const sanitizedColor = color.toLowerCase();
        const colorElement = document.createElement('span')
        colorElement.setAttribute('data-axel-colorpicker', sanitizedColor)
        colorElement.style.cssText = 'min-width: 10px; min-height: 10px; cursor: pointer;'
        colorElement.style.backgroundColor = sanitizedColor
        controls.push(colorElement)
        colorGroupElement.appendChild(colorElement)
      })
      element.appendChild(colorGroupElement)
    })

    // create object for color picker UI element
		const colorPickerObj: AxelColorPickerObject = {
      element: element,
			controls: controls,
      options: options,
      selectedColor: colorToSelect,
			selectColor: selectColor
    }

		// set initial state
		colorToSelect && selectColor(colorToSelect)

    // append event listeners for controls (color elements)
		colorPickerObj.controls.forEach(el => {
			el.addEventListener('click', (ev => {
        const color = el.attributes.getNamedItem('data-axel-colorpicker')?.value || null
        selectColor(color)
        onSelect(color, colorPickerObj.options.name)
			}))
		})

    // add tabs object to the static `allTabs` list
		this.allObjects.push(colorPickerObj)

		return colorPickerObj
  }

  /**
   * Opens a tab in specific tab group
   * 
   * @param colorPickerName name of color picker instance
   * @param color color in any valid css format
   */
	static selectColor(colorPickerName: string, color: string) {
    const colorPicker = this.getObject(colorPickerName)
    colorPicker.selectColor(color)
  }
}