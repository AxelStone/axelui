import {
  AxelAccordionObject,
  AxelColorPickerObject,
  AxelElementObjectBase,
  AxelLayoutObject,
  AxelSelectObject,
  AxelSliderObject,
  AxelSwitcherObject,
  AxelTabsObject,
} from "./axel-types";

/**
 * Static base class to be extended by all Axel UI element classes
 */
export abstract class AxelElementBaseStatic {
  /**
   * All initialized UI objects are stored here
   */
  protected static allObjects: AxelElementObjectBase[] = [];

  /**
   * Get list of all initialized UI objects
   *
   * @param names names of the objects to return
   * @returns initialized objects
   */
  protected static _getObjects<T extends AxelElementObjectBase>(
    names?: string[]
  ): T[] {
    if (!names) return this.allObjects as T[];

    return this.allObjects.filter((object) =>
      names.includes(object.options.name as string)
    ) as T[];
  }

  /**
   * Get single initialized UI object
   *
   * @param name names of the object to return, if missing the first object is returned
   * @returns initialized object
   */
  protected static _getObject<T extends AxelElementObjectBase>(
    name?: string
  ): T {
    if (!name) return this.allObjects[0] as T;

    return this.allObjects.find((object) => object.options.name === name) as T;
  }
}

export abstract class AxelLayoutStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelLayoutObject>;
  static getObjects = this._getObjects<AxelLayoutObject>;
}

export abstract class AxelSwitcherStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelSwitcherObject>;
  static getObjects = this._getObjects<AxelSwitcherObject>;
}

export abstract class AxelSliderStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelSliderObject>;
  static getObjects = this._getObjects<AxelSliderObject>;
}

export abstract class AxelTabsStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelTabsObject>;
  static getObjects = this._getObjects<AxelTabsObject>;
}

export abstract class AxelAccordionStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelAccordionObject>;
  static getObjects = this._getObjects<AxelAccordionObject>;
}

export abstract class AxelSelectStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelSelectObject>;
  static getObjects = this._getObjects<AxelSelectObject>;
}

export abstract class AxelColorPickerStatic extends AxelElementBaseStatic {
  static getObject = this._getObject<AxelColorPickerObject>;
  static getObjects = this._getObjects<AxelColorPickerObject>;
}

export function onClickOutsideOf(
  element: HTMLElement,
  callback: (event: MouseEvent) => void,
  prevCallback?: (event: MouseEvent) => void 
) {
  prevCallback && document.removeEventListener('click', prevCallback)
  const onClickOutsideCallback = (event) => {
    if (!element.contains(event.target as Node)) {
      callback(event);
    }
  }
  document.addEventListener('click', onClickOutsideCallback);

  return onClickOutsideCallback
}
