import typescript from 'rollup-plugin-typescript2';

/**
 * @type {import('rollup').RollupOptions}
 */
export default {
  input: 'src/index.ts',
  output: {
    file: 'dist/axelui.js',
    format: 'iife',
    name: 'AxelUI',
  },
  plugins: [
    typescript({
      tsconfig: "tsconfig.json",
    })
  ]
};