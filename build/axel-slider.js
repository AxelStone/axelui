import { AxelSliderStatic } from "./axel";
export class AxelSlider extends AxelSliderStatic {
    static defaultOptions = {
        name: null,
        startAt: 1
    };
    static init(selector, userOptions = {}) {
        let element = document.querySelector(selector);
        let pages = Array.from(element.querySelectorAll('.page'));
        let options = { ...this.defaultOptions, ...userOptions };
        let currentPage = options.startAt;
        element.insertAdjacentHTML('beforeend', `<div class="axel-pagination">
      <button class="axel-arrow axel-left">&#5176;</button>
      <span class="axel-page">
        <span class="axel-current">${currentPage}</span>/<span class="axel-total">${pages.length}</span>
      </span>
      <button class="axel-arrow axel-right">&#5171;</button>
    </div>`);
        let slider = {
            element: element,
            options: options,
            pages: pages,
            currentPage: currentPage,
            controls: {
                elArrowLeft: element.querySelector('.axel-pagination .axel-left'),
                elArrowRight: element.querySelector('.axel-pagination .axel-right'),
                elCurrentPage: element.querySelector('.axel-pagination .axel-current'),
                elPageTotal: element.querySelector('.axel-pagination .axel-total'),
            },
            slideTo: slideTo
        };
        this.allObjects.push(slider);
        setArrows();
        setPage();
        slider.controls.elArrowLeft.addEventListener('click', (ev) => {
            slider.slideTo(slider.currentPage - 1);
        });
        slider.controls.elArrowRight.addEventListener('click', (ev) => {
            slider.slideTo(slider.currentPage + 1);
        });
        function slideTo(index) {
            if (index <= slider.pages.length && index >= 1) {
                slider.currentPage = index;
                slider.controls.elCurrentPage.innerHTML = (index).toString();
                setArrows();
                setPage();
            }
        }
        function setArrows() {
            if (slider.currentPage === 1) {
                slider.controls.elArrowLeft.classList.add('disabled');
            }
            else {
                slider.controls.elArrowLeft.classList.remove('disabled');
            }
            if (slider.currentPage === slider.pages.length) {
                slider.controls.elArrowRight.classList.add('disabled');
            }
            else {
                slider.controls.elArrowRight.classList.remove('disabled');
            }
        }
        function setPage() {
            slider.pages.forEach((pageEl, index) => {
                if (index === slider.currentPage - 1) {
                    pageEl.style.display = 'block';
                }
                else {
                    pageEl.style.display = 'none';
                }
            });
        }
        return slider;
    }
    static slideTo(sliderName, slideToPageIndex) {
        const slider = this.getObject(sliderName);
        slider.slideTo(slideToPageIndex);
    }
}
