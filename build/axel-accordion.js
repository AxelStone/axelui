import { AxelAccordionStatic } from "./axel";
export class AxelAccordion extends AxelAccordionStatic {
    static initialized = false;
    static defaultOptions = {
        name: null,
        allowMultiple: false,
        defaultOpenedTabs: []
    };
    static init(selector, userOptions = {}) {
        const tabSelector = '[data-axel-accordion*="tab-"]';
        const controlSelector = '[data-axel-accordion="control"]';
        const panelSelector = '[data-axel-accordion="panel"]';
        if (!this.initialized) {
            const style = document.createElement('style');
            style.textContent = tabSelector + ':not(.axel-active) ' + panelSelector + ' { display: none }';
            document.head.appendChild(style);
            this.initialized = true;
        }
        const element = document.querySelector(selector);
        const tabs = Array.from(element.querySelectorAll(tabSelector));
        const options = { ...this.defaultOptions, ...userOptions };
        const tabsToOpen = options.defaultOpenedTabs;
        const switchTab = (tabName) => {
            accordionObj.tabs.forEach(tab => {
                const openedTabNames = accordionObj.openedTabNames;
                if (tab.attributes.getNamedItem('data-axel-accordion')?.value === tabName) {
                    if (tab.classList.contains('axel-active')) {
                        tab.classList.remove('axel-active');
                        accordionObj.openedTabNames = openedTabNames.filter(name => name !== tabName);
                    }
                    else {
                        tab.classList.add('axel-active');
                        openedTabNames.push(tabName);
                    }
                }
                else {
                    if (options.allowMultiple !== true) {
                        tab.classList.remove('axel-active');
                        accordionObj.openedTabNames = openedTabNames.filter(name => name !== tabName);
                    }
                }
            });
        };
        const closeAllTabs = () => {
            accordionObj.tabs.forEach(tab => {
                tab.classList.remove('axel-active');
            });
            accordionObj.openedTabNames = [];
        };
        const accordionObj = {
            element: element,
            tabs: tabs,
            options: options,
            openedTabNames: tabsToOpen,
            switchTab: switchTab,
            closeAllTabs: closeAllTabs
        };
        tabsToOpen.forEach((tabName) => {
            switchTab(tabName);
        });
        accordionObj.tabs.forEach(tabEl => {
            const tabControl = tabEl.querySelector(controlSelector);
            tabControl.addEventListener('click', (ev => {
                const tabName = tabEl.attributes.getNamedItem('data-axel-accordion')?.value;
                accordionObj.switchTab(tabName);
            }));
        });
        this.allObjects.push(accordionObj);
        return accordionObj;
    }
    static switchTab(accordionName, tabName) {
        const accordion = this.getObject(accordionName);
        accordion.switchTab(tabName);
    }
    static closeAllTabs(accordionName) {
        const accordion = this.getObject(accordionName);
        accordion.closeAllTabs();
    }
}
