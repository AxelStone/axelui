import { AxelSwitcherStatic } from "./axel";
export class AxelSwitcher extends AxelSwitcherStatic {
    static initialized = false;
    static defaultOptions = {
        name: null,
        defaultState: 'off',
        mode: 'binary'
    };
    static init(selector, onSwitch, userOptions = {}) {
        const controlSelector = '[data-axel-switcher="control"]';
        if (!this.initialized) {
            const style = document.createElement('style');
            const line1 = selector + ' { min-width: 15px; display: inline-block; } ';
            const line2 = selector + '.axel-on { text-align: right; } ';
            const line3 = selector + '.axel-neutral { text-align: center; } ';
            const line4 = selector + '.axel-off { text-align: left; } ';
            const line5 = selector + ' [data-axel-switcher="control"] { display: inline-block; min-width: 10px; min-height: 10px; } ';
            style.textContent = line1 + line2 + line3 + line4 + line5;
            document.head.appendChild(style);
            this.initialized = true;
        }
        const element = document.querySelector(selector);
        const existingControl = element.querySelector(controlSelector);
        const options = { ...this.defaultOptions, ...userOptions };
        const control = existingControl ?? (() => {
            const generatedControl = document.createElement('span');
            generatedControl.setAttribute('data-axel-switcher', 'control');
            element.appendChild(generatedControl);
            return generatedControl;
        })();
        const switchState = (state = undefined) => {
            const switchToState = state ?? (() => {
                if (switcherObj.state === 'on')
                    return 'off';
                else if (switcherObj.state === 'neutral')
                    return 'on';
                else
                    return switcherObj.options.mode === 'binary' ? 'on' : 'neutral';
            })();
            if (switchToState === 'on') {
                switcherObj.element.classList.add('axel-on');
                switcherObj.element.classList.remove('axel-neutral');
                switcherObj.element.classList.remove('axel-off');
            }
            else if (switchToState === 'neutral') {
                switcherObj.element.classList.remove('axel-on');
                switcherObj.element.classList.add('axel-neutral');
                switcherObj.element.classList.remove('axel-off');
            }
            else if (switchToState === 'off') {
                switcherObj.element.classList.remove('axel-on');
                switcherObj.element.classList.remove('axel-neutral');
                switcherObj.element.classList.add('axel-off');
            }
            switcherObj.state = switchToState;
            onSwitch(switchToState, switcherObj.options.name);
            return switchToState;
        };
        const switcherObj = {
            element: element,
            control: control,
            options: options,
            state: options.defaultState,
            switch: switchState,
        };
        switcherObj.switch(switcherObj.state);
        switcherObj.element.addEventListener('click', (ev => {
            const newState = switcherObj.switch();
            onSwitch(newState, switcherObj.options.name);
        }));
        this.allObjects.push(switcherObj);
        return switcherObj;
    }
    static switch(switcherName, state) {
        const switcher = this.getObject(switcherName);
        switcher.switch(state);
    }
}
