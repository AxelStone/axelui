import { AxelSelectStatic } from "./axel";
export class AxelSelect extends AxelSelectStatic {
    static initialized = false;
    static init(selector, userOptions = {}) {
        const defaultOptions = {
            name: null,
            allowMultiple: true,
            defaultSelectedOptions: []
        };
        const selectSelector = '[data-axel-select="select"]';
        const controlSelector = '[data-axel-select="control"]';
        const panelSelector = '[data-axel-select="panel"]';
        const valuesSelector = '[data-axel-select="values"]';
        const optionsSelectors = '[data-axel-select*="option-"]';
        if (!this.initialized) {
            const style = document.createElement('style');
            style.textContent = selectSelector + ':not(.axel-active) ' + panelSelector + ' { display: none }';
            document.head.appendChild(style);
            this.initialized = true;
        }
        const element = document.querySelector(selector);
        const control = element.querySelector(controlSelector);
        const panel = element.querySelector(panelSelector);
        const optionEls = Array.from(panel.querySelectorAll(optionsSelectors));
        const values = element.querySelector(valuesSelector);
        const options = { ...defaultOptions, ...userOptions };
        const optionsToSelect = options.defaultSelectedOptions;
        const switchOption = (optionNameToSwitch, state) => {
            optionEls.forEach(optionEl => {
                if (optionEl.attributes.getNamedItem('data-axel-select')?.value !== optionNameToSwitch)
                    return;
                function regenerateSelectedValues(optionEl) {
                    values.innerHTML = '';
                    selectObj.selectedOptions.forEach((optionName) => {
                        const addedOptionEl = document.createElement('span');
                        addedOptionEl.setAttribute('data-axel-select', optionName);
                        addedOptionEl.innerHTML = optionEl.innerHTML;
                        values.appendChild(addedOptionEl);
                    });
                }
                function selectOption() {
                    const isAlreadySelected = selectObj.selectedOptions.includes(optionNameToSwitch);
                    if (isAlreadySelected)
                        return;
                    optionEl.classList.add('axel-active');
                    selectObj.selectedOptions.push(optionNameToSwitch);
                    regenerateSelectedValues(optionEl);
                }
                function deselectOption() {
                    optionEl.classList.remove('axel-active');
                    selectObj.selectedOptions = selectObj.selectedOptions.filter((optionName) => optionName !== optionNameToSwitch);
                    regenerateSelectedValues(optionEl);
                }
                if (state === true)
                    selectOption();
                else if (state === false)
                    deselectOption();
                else if (selectObj.selectedOptions.includes(optionNameToSwitch))
                    deselectOption();
                else
                    selectOption();
            });
        };
        const selectObj = {
            element: element,
            options: options,
            selectedOptions: [],
            switchOption: switchOption
        };
        optionsToSelect.forEach((optionName) => switchOption(optionName, true));
        control.addEventListener('click', (ev) => {
            element.classList.toggle('axel-active');
        });
        optionEls.forEach((optionEl) => {
            optionEl.addEventListener('click', (ev) => {
                const optionNameToSwitch = optionEl.attributes.getNamedItem('data-axel-select')?.value;
                if (!optionNameToSwitch)
                    return;
                if (options.allowMultiple) {
                    switchOption(optionNameToSwitch);
                }
                else {
                    selectObj.selectedOptions.forEach((optionName) => {
                        if (optionName !== optionNameToSwitch) {
                            switchOption(optionName, false);
                        }
                    });
                    switchOption(optionNameToSwitch, true);
                }
                element.classList.remove('axel-active');
            });
        });
        this.allObjects.push(selectObj);
        return selectObj;
    }
    static switchOption(optionName, state, selectName) {
        const select = this.getObject(selectName);
        select.switchOption(optionName, state);
    }
}
