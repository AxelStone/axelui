import { AxelTabsStatic } from "./axel";
export class AxelTabs extends AxelTabsStatic {
    static initialized = false;
    static init(selector, userOptions = {}) {
        const defaultOptions = {
            name: null,
            defaultOpenedTab: null
        };
        const controlsSelector = '[data-axel-tabs="controls"]';
        const panelsSelector = '[data-axel-tabs="panels"]';
        const tabsSelectors = '[data-axel-tabs*="tab-"]';
        if (!this.initialized) {
            const style = document.createElement('style');
            style.textContent = panelsSelector + ' ' + tabsSelectors + ':not(.axel-active) { display: none }';
            document.head.appendChild(style);
            this.initialized = true;
        }
        const element = document.querySelector(selector);
        const ctr = element.querySelector(controlsSelector);
        const pan = element.querySelector(panelsSelector);
        const controls = Array.from(ctr.querySelectorAll(tabsSelectors));
        const panels = Array.from(pan.querySelectorAll(tabsSelectors));
        const options = { ...defaultOptions, ...userOptions };
        const tabToOpen = options.defaultOpenedTab;
        const openTab = (tabName) => {
            tabsObj.panels.forEach(tab => {
                if (tab.attributes.getNamedItem('data-axel-tabs')?.value === tabName) {
                    tab.classList.add('axel-active');
                }
                else {
                    tab.classList.remove('axel-active');
                }
            });
            tabsObj.controls.forEach(ctr => {
                if (ctr.attributes.getNamedItem('data-axel-tabs')?.value === tabName) {
                    ctr.classList.add('axel-active');
                }
                else {
                    ctr.classList.remove('axel-active');
                }
            });
        };
        const tabsObj = {
            element: element,
            controls: controls,
            panels: panels,
            options: options,
            openedTabName: tabToOpen,
            openTab: openTab
        };
        tabToOpen && openTab(tabToOpen);
        tabsObj.controls.forEach(el => {
            el.addEventListener('click', (ev => {
                const tabName = el.attributes.getNamedItem('data-axel-tabs')?.value;
                tabsObj.openTab(tabName);
                tabsObj.openedTabName = tabName;
            }));
        });
        this.allObjects.push(tabsObj);
        return tabsObj;
    }
    static openTab(tabGroupName, tabName) {
        const tabs = this.getObject(tabGroupName);
        tabs.openTab(tabName);
    }
}
