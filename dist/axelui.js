(function () {
  'use strict';

  class AxelElementBaseStatic {
      static allObjects = [];
      static _getObjects(names) {
          if (!names)
              return this.allObjects;
          return this.allObjects.filter((object) => names.includes(object.options.name));
      }
      static _getObject(name) {
          if (!name)
              return this.allObjects[0];
          return this.allObjects.find((object) => object.options.name === name);
      }
  }
  class AxelAccordionStatic extends AxelElementBaseStatic {
      static getObject = (this._getObject);
      static getObjects = (this._getObjects);
  }
  class AxelSelectStatic extends AxelElementBaseStatic {
      static getObject = (this._getObject);
      static getObjects = (this._getObjects);
  }
  function onClickOutsideOf(element, callback, prevCallback) {
      prevCallback && document.removeEventListener('click', prevCallback);
      const onClickOutsideCallback = (event) => {
          if (!element.contains(event.target)) {
              callback(event);
          }
      };
      document.addEventListener('click', onClickOutsideCallback);
      return onClickOutsideCallback;
  }

  class AxelSelect extends AxelSelectStatic {
      static initialized = false;
      static listeners = {
          onControlClick: (selectObj) => () => {
              selectObj.element.classList.toggle('axel-active');
          },
          onOptionClick: (selectObj, optionEl, options) => () => {
              const optionNameToSwitch = optionEl.attributes.getNamedItem('data-axel-select')?.value;
              if (!optionNameToSwitch)
                  return;
              if (options.allowMultiple) {
                  selectObj.switchOption(optionNameToSwitch, undefined, true);
              }
              else {
                  selectObj.selectedOptions.forEach((optionName) => {
                      if (optionName !== optionNameToSwitch) {
                          selectObj.switchOption(optionName, false, true);
                      }
                  });
                  selectObj.switchOption(optionNameToSwitch, true, true);
              }
              selectObj.options.closeOnSelect && selectObj.element.classList.remove('axel-active');
          },
          onClickOutside: (selectObj) => () => {
              selectObj.element.classList.remove('axel-active');
          }
      };
      static init = (selector, userOptions = {}, onSwitch) => {
          const defaultOptions = {
              name: selector,
              allowMultiple: true,
              closeOnSelect: true,
              textEmpty: '- select -',
              textFull: null,
              defaultSelectedOptions: []
          };
          const selectSelector = '[data-axel-select="select"]';
          const controlSelector = '[data-axel-select="control"]';
          const panelSelector = '[data-axel-select="panel"]';
          const valuesSelector = '[data-axel-select="values"]';
          const optionsSelectors = '[data-axel-select*="option-"]';
          if (!this.initialized) {
              const style = document.createElement('style');
              style.textContent = selectSelector + ':not(.axel-active) ' + panelSelector + ' { display: none }';
              document.head.appendChild(style);
              this.initialized = true;
          }
          const element = document.querySelector(selector);
          const control = element.querySelector(controlSelector);
          const panel = element.querySelector(panelSelector);
          const optionEls = Array.from(panel.querySelectorAll(optionsSelectors));
          const values = element.querySelector(valuesSelector);
          const options = { ...defaultOptions, ...userOptions };
          const optionsToSelect = options.defaultSelectedOptions;
          const previousObj = this.getObject(options.name);
          element.setAttribute('data-axel-select', 'select');
          const switchOptionFn = (optionName, newState = undefined, triggerCallback = false) => {
              function regenerateSelectedValues() {
                  if (!values)
                      return;
                  if (selectObj.selectedOptions.length === 0) {
                      values.innerHTML = options.textEmpty;
                  }
                  else {
                      if (selectObj.options.textFull) {
                          values.innerHTML = selectObj.options.textFull;
                      }
                      else {
                          values.innerHTML = '';
                          selectObj.selectedOptions.forEach((optionName) => {
                              const selectedOption = optionEls.find((optionEl) => optionEl.attributes.getNamedItem('data-axel-select')?.value === optionName);
                              const generatedOptionEl = document.createElement('span');
                              generatedOptionEl.setAttribute('data-axel-select', optionName);
                              generatedOptionEl.innerHTML = selectedOption.innerHTML;
                              values.appendChild(generatedOptionEl);
                          });
                      }
                  }
              }
              function selectOption(optionEl) {
                  const isAlreadySelected = selectObj.selectedOptions.includes(optionName);
                  if (isAlreadySelected)
                      return;
                  optionEl.classList.add('axel-active');
                  selectObj.selectedOptions.push(optionName);
                  regenerateSelectedValues();
                  triggerCallback && onSwitch(optionName, true, selectObj.selectedOptions, optionEl);
              }
              function deselectOption(optionEl) {
                  optionEl.classList.remove('axel-active');
                  selectObj.selectedOptions = selectObj.selectedOptions.filter((optName) => optName !== optionName);
                  regenerateSelectedValues();
                  triggerCallback && onSwitch(optionName, false, selectObj.selectedOptions, optionEl);
              }
              optionEls.forEach(optionEl => {
                  if (optionEl.attributes.getNamedItem('data-axel-select')?.value !== optionName)
                      return;
                  if (newState === true)
                      selectOption(optionEl);
                  else if (newState === false)
                      deselectOption(optionEl);
                  else if (selectObj.selectedOptions.includes(optionName))
                      deselectOption(optionEl);
                  else
                      selectOption(optionEl);
              });
          };
          const switchOptionsFn = (optionNames, newState = true, triggerCallbacks = false) => {
              optionEls.forEach((optionEl) => {
                  selectObj.selectedOptions = [];
                  const optionName = optionEl.attributes.getNamedItem('data-axel-select')?.value;
                  if (optionNames.includes(optionName)) {
                      optionEl.classList.toggle('axel-active', newState);
                      newState !== false ?
                          selectObj.selectedOptions.push(optionName) :
                          selectObj.selectedOptions = selectObj.selectedOptions.filter((optName) => optName !== optionName);
                  }
                  else {
                      optionEl.classList.toggle('axel-active', !newState);
                      selectObj.selectedOptions = selectObj.selectedOptions.filter((optName) => optName !== optionName);
                  }
                  triggerCallbacks && onSwitch(optionName, newState, selectObj.selectedOptions, optionEl);
              });
          };
          const selectObj = {
              element: element,
              options: options,
              selectedOptions: [],
              listeners: null,
              switchOption: switchOptionFn,
              switchOptions: switchOptionsFn,
          };
          const onClickOutsideCallback = onClickOutsideOf(element, this.listeners.onClickOutside(selectObj), previousObj?.listeners.onClickOutside);
          const listeners = {
              onControlClick: this.listeners.onControlClick(selectObj),
              onClickOutside: onClickOutsideCallback,
              options: {}
          };
          if (!optionsToSelect.length)
              values.innerHTML = options.textEmpty;
          else
              optionsToSelect.forEach((optionName) => selectObj.switchOption(optionName, true));
          previousObj?.listeners.onControlClick &&
              control.removeEventListener('click', previousObj.listeners.onControlClick);
          control.addEventListener('click', listeners.onControlClick);
          optionEls.forEach((optionEl) => {
              const optionName = optionEl.getAttribute('data-axel-select');
              listeners.options[optionName] = this.listeners.onOptionClick(selectObj, optionEl, options);
              previousObj?.listeners.options[optionName] &&
                  optionEl.removeEventListener('click', previousObj.listeners.options[optionName]);
              optionEl.addEventListener('click', listeners.options[optionName]);
          });
          selectObj.listeners = listeners;
          this.allObjects = this.allObjects.filter((object) => object.options.name !== options.name).concat(selectObj);
          return selectObj;
      };
      static switchOption = (optionName, state, selectName) => {
          const select = this.getObject(selectName);
          select.switchOption(optionName, state);
      };
      static switchOptions = (optionNames, state, selectName) => {
          const select = this.getObject(selectName);
          select.switchOptions(optionNames, state);
      };
  }

  class AxelAccordion extends AxelAccordionStatic {
      static initialized = false;
      static init(selector, userOptions = {}) {
          const defaultOptions = {
              name: selector,
              allowMultiple: false,
              defaultOpenedTabs: []
          };
          const tabSelector = '[data-axel-accordion*="tab-"]';
          const controlSelector = '[data-axel-accordion="control"]';
          const panelSelector = '[data-axel-accordion="panel"]';
          if (!this.initialized) {
              const style = document.createElement('style');
              style.textContent = tabSelector + ':not(.axel-active) ' + panelSelector + ' { display: none }';
              document.head.appendChild(style);
              this.initialized = true;
          }
          const element = document.querySelector(selector);
          const tabs = Array.from(element.querySelectorAll(tabSelector));
          const options = { ...defaultOptions, ...userOptions };
          const tabsToOpen = options.defaultOpenedTabs;
          const switchTab = (tabName) => {
              accordionObj.tabs.forEach(tab => {
                  const openedTabNames = accordionObj.openedTabNames;
                  if (tab.attributes.getNamedItem('data-axel-accordion')?.value === tabName) {
                      if (tab.classList.contains('axel-active')) {
                          tab.classList.remove('axel-active');
                          accordionObj.openedTabNames = openedTabNames.filter(name => name !== tabName);
                      }
                      else {
                          tab.classList.add('axel-active');
                          openedTabNames.push(tabName);
                      }
                  }
                  else {
                      if (options.allowMultiple !== true) {
                          tab.classList.remove('axel-active');
                          accordionObj.openedTabNames = openedTabNames.filter(name => name !== tabName);
                      }
                  }
              });
          };
          const closeAllTabs = () => {
              accordionObj.tabs.forEach(tab => {
                  tab.classList.remove('axel-active');
              });
              accordionObj.openedTabNames = [];
          };
          const accordionObj = {
              element: element,
              tabs: tabs,
              options: options,
              openedTabNames: tabsToOpen,
              switchTab: switchTab,
              closeAllTabs: closeAllTabs
          };
          tabsToOpen.forEach((tabName) => {
              switchTab(tabName);
          });
          accordionObj.tabs.forEach(tabEl => {
              const tabControl = tabEl.querySelector(controlSelector);
              tabControl.addEventListener('click', () => {
                  const tabName = tabEl.attributes.getNamedItem('data-axel-accordion')?.value;
                  accordionObj.switchTab(tabName);
              });
          });
          this.allObjects.push(accordionObj);
          return accordionObj;
      }
      static switchTab(accordionName, tabName) {
          const accordion = this.getObject(accordionName);
          accordion.switchTab(tabName);
      }
      static closeAllTabs(accordionName) {
          const accordion = this.getObject(accordionName);
          accordion.closeAllTabs();
      }
  }

  window.AxelSelect = AxelSelect;
  window.AxelAccordion = AxelAccordion;

})();
